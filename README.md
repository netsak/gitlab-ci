# GitLab CI/CD Scripts

Common scripts for GitLab CI/CD which can be included into the repository `.gitlab-ci.yml` to standardize the process.

## Docker
The standard docker build (latest, tags and branches)
```yaml
include:
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-master.yml'
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-tags.yml'
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-branches.yml'
```
Build the master branch:
```yaml
include:
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-master.yml'
```
Build every branch other than master:
```yaml
include:
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-branches.yml'
```
Build every tag:
```yaml
include:
    - 'https://gitlab.com/netsak/gitlab-ci/raw/master/docker-build-tags.yml'
```

